/*
 *	Author: Jace Young (onurmark1@gmail.com)
 *
 *  Compile: gcc -o pcaps pcaps.c -lpcap
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <getopt.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <net/ethernet.h>
#include <netinet/ip.h>

#include <pcap/pcap.h>

#define LINE_LEN 16
#define FNAME_LEN 32

#define ACT_SEND_PACKET 0
#define ACT_DUMP_PACKET 1

const char *program_name;
const char *interface = NULL;
const char *pcapfile = NULL;
struct ether_addr *dst_ether = NULL;
struct ether_addr *src_ether = NULL;
int dumplevel = 0;

static void print_usage(FILE *stream, int exit_code)
{
	fprintf(stream, "Usage: %s -i [eth_device] -f [pcap_file]\n", program_name);
	fprintf(stream, 
				"\t-h      --help       Display this usage.\n"
				"\t-i      --interface  Send interface.\n"
				"\t-f      --file       Pcap file.\n"
				"\t-d      --dst-mac    Spoofing destination MAC address.\n"
				"\t-s      --src-mac    Spoofing source MAC address.\n"
				"\t-l      --dumplevel  Packet dump level.\n"
	);
}

static int parse_option(int argc, char **argv)
{
	int next_option;
	struct ether_addr *buf;
	
	const char *const short_options = "hi:f:d:s:l:";
	const struct option long_options[] = {
		{ "help",		0,	NULL,	'h' },
		{ "interface",	1,	NULL,	'i' },
		{ "file",		1,	NULL,	'f' },
		{ "dst-mac",	1,	NULL,	'd' },
		{ "src-mac",	1,	NULL,	's' },
		{ "dumplevel",	1,	NULL,	'l' },
		{ NULL, 0, NULL, 0 }
	};

	program_name = argv[0];

	do {
		next_option = getopt_long(argc, argv, short_options, long_options, NULL);

		switch (next_option) {
		case 'h' :
			print_usage(stdout, 0);
			exit(0);
			break;

		case 'i' :
			interface = strdup(optarg);
			break;

		case 'f' :
			pcapfile = strdup(optarg);
			break;

		case 'd' :
			/* ether_aton() returned statically allocated buffer. */
			buf = (struct ether_addr *)ether_aton(optarg);
			if (!buf) {
				fprintf(stderr, "Wrong format: -d %s\n", optarg);
				abort();
			}
			dst_ether = (struct ether_addr *)malloc(sizeof(struct ether_addr));
			memcpy(dst_ether, buf, sizeof(struct ether_addr));
			break;

		case 's' :
			/* ether_aton() returned statically allocated buffer. */
			buf = (struct ether_addr *)ether_aton(optarg);
			if (!buf) {
				fprintf(stderr, "Wrong format: -s %s\n", optarg);
				abort();
			}
			src_ether = (struct ether_addr *)malloc(sizeof(struct ether_addr));
			memcpy(src_ether, buf, sizeof(struct ether_addr));
			break;

		case 'l' :
			dumplevel = atoi(optarg);
			break;

		case '?' :	/* Wrong option. */
			print_usage(stderr, 1);
			abort();
			break;

		case -1:	/* No more option. */
			break;

		default:	/* Unexpected error. */
			abort();
		}
	} while (next_option != -1);

	if (interface == NULL || pcapfile == NULL) {
		fprintf(stderr, "This program is needed more options.\n");
		print_usage(stderr, 0);
		abort();
	}

	return 0;
}

static void dump_packet(struct pcap_pkthdr *header, const u_char *pkt_data)
{
	int i;

	switch (dumplevel) {
	case 1: /* hex dump */
		printf("%ld:%ld (%d)\n", header->ts.tv_sec, header->ts.tv_usec, header->len);
		for (i = 1; i < header->caplen + 1; i++) {
			printf("%.2x ", pkt_data[i - 1]);
			if ( (i % LINE_LEN) == 0) printf("\n");
		}
		printf("\n\n");
	case 0: /* no dump */
	default:
		break;
	}
}

static int resolv_ifindex_by_devname(int sock, const char *devname)
{
	struct ifreq ifr;

	strncpy(ifr.ifr_name, devname, IFNAMSIZ);
	if (ioctl(sock, SIOCGIFINDEX, &ifr) >= 0) {
		return ifr.ifr_ifindex;
	}
	fprintf(stderr, "Error: Can't get ifindex\n");
	return -1;
}

static int send_packet(struct pcap_pkthdr *header, const u_char *pkt_data)
{
	int sock, ret, on = 1;
	struct sockaddr_ll dst;
	struct ether_header *ep;

	ep = (struct ether_header *)pkt_data;
	
	sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (sock > 0) {
		/* set destination address */
		dst.sll_family   = AF_INET;
		dst.sll_protocol = ep->ether_type;
		dst.sll_ifindex  = resolv_ifindex_by_devname(sock, interface);
		dst.sll_hatype   = ARPHRD_ETHER;
		dst.sll_pkttype  = PACKET_OTHERHOST;
		dst.sll_halen    = ETH_ALEN;

		if (src_ether) { /* if set source MAC address by options */
			memcpy(ep->ether_shost, src_ether, ETH_ALEN);
		}

		if (dst_ether) { /* if set destination MAC address by options */
			memcpy(dst.sll_addr, dst_ether, ETH_ALEN);
			memcpy(ep->ether_dhost, dst_ether, ETH_ALEN);
		} else { /* set pcap file's destination MAC address */
			memcpy(dst.sll_addr, ep->ether_dhost, ETH_ALEN);
		}

		ret = sendto(sock, pkt_data, header->len, 0, (struct sockaddr *)&dst, sizeof(dst));
		if (ret < 0) {
			fprintf(stderr, "Error: Can't send packet\n");
		} else {
			dump_packet(header, pkt_data);
		}

		close(sock);
	} else {
		fprintf(stderr, "Error: Can't open socket\n");
		return -1;
	}

	return 0;
}

	
int main(int argc, char *argv[])
{
	pcap_t *pcd;
	struct pcap_pkthdr *header;
	const u_char *pkt_data;
	char strerr[PCAP_ERRBUF_SIZE];
	int ret, fcnt = 0, scnt = 0;

	uid_t user_id;

	/* Check root permission */
	user_id = getuid();
	if (user_id != 0) {
		fprintf(stderr, "This program is needed root permission\n");
		abort();
	}

	ret = parse_option(argc, argv);
	if (!ret) {
		/* read from pcap file */
		pcd = pcap_open_offline(pcapfile, strerr);

		if (pcd != NULL) {
			/* Until EOF */
			while ((ret = pcap_next_ex(pcd, &header, &pkt_data)) >= 0) {
				ret = send_packet(header, pkt_data);
				if (ret) fcnt++; else scnt++;
			}
			pcap_close(pcd);
		} else {
			fprintf(stderr, "Error: Can't open device\n");
		}
	}

	printf("Result: read[%d], success[%d], fail[%d]\n", scnt + fcnt, scnt, fcnt);

	/* deallocation */
	free((char *)interface);
	free((char *)pcapfile);
	if (src_ether) free((struct ether_addr *)src_ether);
	if (dst_ether) free((struct ether_addr *)dst_ether);
}
